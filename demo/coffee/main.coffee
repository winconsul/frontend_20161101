Vue = require 'vue'

new Vue
    el: '#main'
    data: () ->
        message: 'message'
        html: '<b>html</b>'
        visibleIf: true
        visibleShow: true
        numbers: [1..10]
    methods:
        alert: (message) ->
            alert message
