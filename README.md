フロントエンド勉強会 vol.4
===============

操作
---------------

### 準備

```
npm install

```

### ビルド開始

```
npm start
```

### プレゼンを見る

```
cd presentation && npm install && npm start
```
