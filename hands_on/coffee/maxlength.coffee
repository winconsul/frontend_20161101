module.exports =
    bind: () ->
        # 監視を仕掛ける
        # @elはこのディレクティブが設定されている要素
        @listener = () =>
            console.log @el.value
            max = 10

            if true
                @appendMessage "最大#{max}文字までですよ。"

        @el.addEventListener 'keyup', @listener

    unbind: () ->
        # 監視を削除する
        @el.removeEventListener 'keyup', @listener

    update: (newValue, oldValue) ->
        # ディレクティブに渡した値がやってくる
        # TODO この値はどこかにとっておきたい。
        console.log newValue

        @listener()

    # メッセージを登録します
    appendMessage: (message) ->
        # メッセージを格納した要素があれば取り除く
        @el.parentNode.removeChild @messageHolder if @messageHolder?

        # メッセージを登録する
        if message? isnt ''
            @messageHolder = document.createElement 'span'
            @messageHolder.setAttribute 'class', 'text-danger'
            @messageHolder.innerHTML = "#{message}"
            @el.parentNode.appendChild @messageHolder
