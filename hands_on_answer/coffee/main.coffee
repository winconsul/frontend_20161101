Vue = require 'vue'

Vue.directive 'example', require './example'
Vue.directive 'maxlength', require './maxlength'

new Vue
    el: '#main'
    data: () ->
        param: 'parameter'
        abc: 'def'
        value: 'value'

