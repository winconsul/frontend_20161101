module.exports =
    bind: () ->
        # 監視を仕掛ける
        # @elはこのディレクティブが設定されている要素
        @listener = () =>
            # !! updateで保存しておいたmaxLengthと文字列の長さを比較する
            if @el.value.length > @maxLength
                @appendMessage "最大#{@maxLength}文字までですよ。"
            else
                @appendMessage ""

        @el.addEventListener 'keyup', @listener

    unbind: () ->
        # 監視を削除する
        @el.removeEventListener 'keyup', @listener

    update: (newValue, oldValue) ->
        # !! thisに値を保存しておく
        @maxLength = newValue

        @listener()

    # メッセージを登録します
    appendMessage: (message) ->
        # メッセージを格納した要素があれば取り除く
        @el.parentNode.removeChild @messageHolder if @messageHolder?

        # メッセージを登録する
        if message? isnt ''
            # !! @modifiersを見ながら文字色を決める
            # !! メソッド化するのも一つの手です
            textStyle = 'text-info'    if @modifiers.info
            textStyle = 'text-warning' if @modifiers.warn
            textStyle = 'text-danger'  if @modifiers.error or not textStyle? 

            @messageHolder = document.createElement 'span'
            @messageHolder.setAttribute 'class', textStyle
            @messageHolder.innerHTML = "#{message}"
            @el.parentNode.appendChild @messageHolder
