module.exports =
    params: ['p1', 'p2']
    bind: () -> # 初期処理
        console.log 'initialize', @arg, @modifiers, @params

    update: (newValue, oldValue) -> # 値が変更されたとき
        console.log oldValue, '=>', newValue

    unbind: () -> # 終了処理
        console.log 'before destroy'
